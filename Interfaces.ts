export interface HasName {
  name: string;
  getName(): string;
  setName(newName: string): void;
}

export interface University extends HasName {
  getAddress(): string;
  setAddress(newAddress: string): void;
  addFaculty(newFaculty: Faculty): void;
  removeFaculty(faculty: Faculty): void;
}

export interface Faculty extends HasName {
  addTeacher(newTeacher: Teacher): void;
  removeTeacher(teacher: Teacher): void;
  addGroup(newGroup: Group): void;
  removeGroup(group: Group): void;
}

export interface Group extends HasName {
  name: string;
  setFaculty(faculty: Faculty): void;
  getFaculty(): Faculty;
  addStudent(newStudent: Student): void;
  removeStudent(student: Student): void;
}

export interface Teacher extends HasName {
  setFaculty(faculty: Faculty): void;
  getFaculty(): Faculty;
  addCourse(newCourse: Course): void;
  removeCourse(course: Course): void;
}

export interface Course extends HasName {
  getDescription(): string;
  setDescription(newDescription: string): void;
  getProgram(): string;
  setProgram(newProgram: string): void;
}

export interface Student extends HasName {
  setGroup(group: Group): void;
  getGroup(): Group;
}