import { University, Faculty, Group, Teacher, Course, Student } from "./Interfaces";

import {
  UniversityClass,
  FacultyClass,
  GroupClass,
  TeacherClass,
  CourseClass,
  StudentClass
} from "./Classes";

enum CourseLevel {
  Beginner = "Beginner",
  Intermediate = "Intermediate",
  Advanced = "Advanced",
}


const university: University = new UniversityClass(
  'Харківський національний університет радіоелектроники',
  'Україна Харків, просп. Науки, 14'
);
const faculty: Faculty = new FacultyClass('Computer Science');
const teacher: Teacher = new TeacherClass('Ivan Petrov', faculty);
const course: Course = new CourseClass(
  'Machine Learning',
  'field devoted to understanding and building methods that let machines "learn" – that is, methods that leverage data to improve computer performance on some set of tasks.',
  '720 hours'
);
const group: Group = new GroupClass('ITAI', faculty);