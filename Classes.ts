import { University, Faculty, Group, Teacher, Course, Student } from "./Interfaces";
export class UniversityClass implements University {
  name: string;
  private address: string;
  private faculties: Faculty[];

  constructor(name: string, address: string, faculties: Faculty[] = []) {
    this.name = name;
    this.address = address;
    this.faculties = faculties;
  }

  getName(): string {
    return this.name;
  }

  setName(newName: string): void {
    this.name = newName;
  }

  getAddress(): string {
    return this.address;
  }

  setAddress(newAddress: string): void {
    this.address = newAddress;
  }

  addFaculty(newFaculty: Faculty): void {
    this.faculties.push(newFaculty);
  }

  removeFaculty(faculty: Faculty): void {
    const index = this.faculties.indexOf(faculty);
    if (index !== -1) {
      this.faculties.splice(index, 1);
    }
  }
}

export class FacultyClass implements Faculty {
  name: string;
  private teachers: Teacher[];
  private groups: Group[];

  constructor(name: string, teachers: Teacher[] = [], groups: Group[] = []) {
    this.name = name;
    this.teachers = teachers;
    this.groups = groups;
  }

  getName(): string {
    return this.name;
  }

  setName(newName: string): void {
    this.name = newName;
  }

  addTeacher(newTeacher: Teacher): void {
    this.teachers.push(newTeacher);
  }

  removeTeacher(teacher: Teacher): void {
    const index = this.teachers.indexOf(teacher);
    if (index !== -1) {
      this.teachers.splice(index, 1);
    }
  }

  addGroup(newGroup: Group): void {
    this.groups.push(newGroup);
  }

  removeGroup(group: Group): void {
    const index = this.groups.indexOf(group);
    if (index !== -1) {
      this.groups.splice(index, 1);
    }
  }
}



export class GroupClass implements Group {
  name: string;
  private faculty: Faculty;
  private students: Student[];

  constructor(name: string, faculty: Faculty, students: Student[] = []) {
    this.name = name;
    this.faculty = faculty;
    this.students = students;
  }

  getName(): string {
    return this.name;
  }

  setName(newName: string): void {
    this.name = newName;
  }

  setFaculty(faculty: Faculty): void {
    this.faculty = faculty;
  }

  getFaculty(): Faculty {
    return this.faculty;
  }

  addStudent(newStudent: Student): void {
    this.students.push(newStudent);
  }

  removeStudent(student: Student): void {
    const index = this.students.indexOf(student);
    if (index !== -1) {
      this.students.splice(index, 1);
    }
  }
}



export class TeacherClass implements Teacher {
  name: string;
  private faculty: Faculty;
  private courses: Course[];

  constructor(name: string, faculty: Faculty, courses: Course[] = []) {
    this.name = name;
    this.faculty = faculty;
    this.courses = courses;
  }

  getName(): string {
    return this.name;
  }

  setName(newName: string): void {
    this.name = newName;
  }

  setFaculty(faculty: Faculty): void {
    this.faculty = faculty;
  }

  getFaculty(): Faculty {
    return this.faculty;
  }

  addCourse(newCourse: Course): void {
    this.courses.push(newCourse);
  }

  removeCourse(course: Course): void {
    const index = this.courses.indexOf(course);
    if (index !== -1) {
      this.courses.splice(index, 1);
    }
  }
}



export class CourseClass implements Course {
  name: string;
  private description: string;
  private program: string;

  constructor(name: string, description: string, program: string) {
    this.name = name;
    this.description = description;
    this.program = program;
  }

  getName(): string {
    return this.name;
  }

  setName(newName: string): void {
    this.name = newName;
  }

  getDescription(): string {
    return this.description;
  }

  setDescription(newDescription: string): void {
    this.description = newDescription;
  }

  getProgram(): string {
    return this.program;
  }

  setProgram(newProgram: string): void {
    this.program = newProgram;
  }
}



export class StudentClass implements Student {
  name: string;
  private group: Group;

  constructor(name: string, group: Group) {
    this.name = name;
    this.group = group;
  }

  getName(): string {
    return this.name;
  }

  setName(newName: string): void {
    this.name = newName;
  }

  setGroup(group: Group): void {
    if (this.group) {
      this.group.removeStudent(this);
    }

    group.addStudent(this);
    this.group = group;
  }

  getGroup(): Group {
    return this.group;
  }
}